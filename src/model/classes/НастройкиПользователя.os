#use fs

Перем ПользовательID;
Перем КаталогПользователя;
Перем ПутьКФайлуНастроек;
Перем НастройкиПользователя;

Перем ВремяАктуальности;

Процедура ПриСозданииОбъекта(пПользовательID)
	ПользовательID = пПользовательID;
	КаталогПользователя = МенеджерНастроекПользователей.КаталогПользователя(ПользовательID);
	ПутьКФайлуНастроек = МенеджерНастроекПользователей.ПутьКФайлуНастроекПользователя(ПользовательID);
	Прочитать();
КонецПроцедуры

Функция Пользователь() Экспорт
	Возврат ПользовательID;
КонецФункции

Функция ИмяФайла() Экспорт
	Возврат ПутьКФайлуНастроек;
КонецФункции

Функция НастройкиПользователя() Экспорт
	Возврат НастройкиПользователя;
КонецФункции

Процедура Записать() Экспорт

	ФС.ОбеспечитьКаталог(КаталогПользователя);
	НастройкиПользователя.Вставить("date_update", ТекущаяДата());
	
	Текст = НастройкиПриложения.Парсер.ЗаписатьJSON(НастройкиПользователя);
	
	ОбщегоНазначения.ЗаписатьТекстовыйФайл(ПутьКФайлуНастроек, Текст);

КонецПроцедуры

Функция ПолучитьЗначение(ИмяСвойства) Экспорт
	Значение = НастройкиПользователя[ИмяСвойства];
	Возврат Значение;
КонецФункции

Функция ПолучитьЗначения(мКлючи) Экспорт
	
	Соответствие = Новый Соответствие;
	
	Для Каждого Ключ Из мКлючи Цикл
		Соответствие.Вставить(Ключ, НастройкиПользователя[Ключ]);
	КонецЦикла;
	
	Возврат Соответствие;

КонецФункции

Процедура УстановитьЗначение(ИмяСвойства, Значение) Экспорт
	
	ВремяАктуальности = ТекущаяДата();

	НастройкиПользователя.Вставить(ИмяСвойства, Значение);

	Записать();

КонецПроцедуры

Процедура УстановитьЗначения(Соотвествие) Экспорт
	
	ВремяАктуальности = ТекущаяДата();

	Для Каждого КЗ Из Соотвествие Цикл
		НастройкиПользователя.Вставить(КЗ.Ключ, КЗ.Значение);
	КонецЦикла;

	Записать();

КонецПроцедуры

Процедура Прочитать() Экспорт
	
	НастройкиПользователя = ПрочитатьНайстрокиИзХранилища();
	
КонецПроцедуры

Функция ПрочитатьНайстрокиИзХранилища()
	
	ВремяАктуальности = ТекущаяДата();

	Если НЕ ФС.ФайлСуществует(ПутьКФайлуНастроек) Тогда
		Возврат НовыеНастройки();
	КонецЕсли;
	
	Текст = ОбщегоНазначения.ПрочитатьТекстовыйФайл(ПутьКФайлуНастроек);
	Соответствие = НастройкиПриложения.Парсер.ПрочитатьJSON(Текст);
	
	Если ЗначениеЗаполнено(Соответствие) Тогда
		Возврат Соответствие;
	Иначе
		Возврат НовыеНастройки();
	КонецЕсли;
	
КонецФункции

Функция НовыеНастройки()

	соотв = Новый Соответствие();
	соотв.Вставить("userID", ПользовательID);
	соотв.Вставить("date_update", ТекущаяДата());
	
	Возврат соотв;

КонецФункции