# Где развернуть бота
Я пошел по пути "бот на своем сервере, в докере"  
Потому что хотелось попробовать развернуть его не где-то в heroku, а у себя.  
Потому что функциональность бота предполагала взаимодействие с сервером.

## Что потребовалось
1. linux (в моем случае - ubuntu 18.04)
2. docker
3. nginx
4. telegrambot

## TelegramBot

> Примечание (на случай невнимательности нечитателей документации):  
 *ТЕЛЕГРАМ работает только по https и только на стандартный портах 80, 433, 8080, 8443*.  

Так как телеграмм работает только по безопасному протоколу, то придется создать ssl сертификат. Если сертификат будет "бесплатный, самоподписанный", то его публичную часть нужно передать боту вместе с установкой вебхука. Если нет, то передавать его не нужно. (О создании сертификата [здесь]())  

Регистрируем WebHook
``` bash
sudo curl \
      -F "url=https://{мой_белый_IP}/{ИдентификаторБота}" \
      -F "certificate=@/etc/ssl/private/myhost.cert" \
    https://api.telegram.org/bot{TOKEN}/setWebhook
```
Если все хорошо, то мы должны получить ответ
``` json
{"ok":true,"result":true,"description":"Webhook was set"}
```

## SSL - самоподписанный сертификат

Создадим файл с конфигом `myhost.conf`
``` 
distinguished_name=req
[SAN]
subjectAltName = @alt_names
[alt_names]
DNS.1 = 123.45.67.89
DNS.2 = myhost
```
Генерируем ключи (секретную `myhost.key` и публичную `myhost.cert` части)
``` bash
openssl genrsa -out myhost.key 2048

openssl req -new -x509 -key myhost.key -out myhost.cert -days 3650 -subj /CN=myhost -extensions SAN -config myhost.conf
```
Располагаем файлы в папку `/etc/ssl/private`  

## Nginx
Спрячем бота на реверс-прокси и пропишем ssl  

В файле `/etc/nginx/sites-available/default` разрешаем ssl (добавляем строки)
```
listen 443 ssl default_server;
listen [::]:443 ssl default_server;
```
```
ssl_certificate     /etc/ssl/private/myhost.cert;
ssl_certificate_key /etc/ssl/private/myhost.key;
```

... и прописываем прокси (добавляем блок `location`):
``` 
location /mystrangename/ {
        rewrite ^/mystrangename/(.*) /$1 break;
        proxy_pass http://localhost:5000;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forward-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forward-Proto http;
        proxy_set_header X-Nginx-Proxy true;
}
```
Здесь `mystrangename` - любая добавка к url, http://localhost:5000 - адрес и порт докер-контейнера.  
Т.е. теперь вместо обращения к боту http://localhost:5000 или http(s)://myhost:5000, мы будем обращаться `http(s)://{IP or Name}/mystrangename`, а значит адрес WebHook'a при настройе бота нужно исправить.  
Адрес должен иметь вид: `https://{мой_белый_IP}/mystrangename/{ИдентификаторБота}`.  
Меняем адрес и перерегистрируем заново WebHook с модифицированным адресом и c SSL сертификатом.

## Docker
### Сборка образа
Выполняется скриптом `docker_build`  

*На момент описания установка пакетов/зависимостей закоментирована. Пакеты установлены в каталог `src/oscript_modules`. Это временное решение для того, чтобы была возможность использовать модифицированные библиотеки, еще не опубликованные в https://hub.oscript.io версии.*

### Запуск контейнера
Выполняется скриптом `docker_run`  

*Примечания:*
- `--network=host` - порт открываем только для `localhost`, т.к. реализован прокси и порт не требуется пробрасывать во внешние сети
- `--env-file ~/.secrets/$docker_name` - все переменные окружения, в т.ч. секреты, описываем одноименном файле

### Развертывание (автосборка и запуск)
Выполняется с помощью Jenkins.

---
[README.md](/README.md)