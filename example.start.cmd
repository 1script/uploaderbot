
@REM Для отладки
@REM SET TELEGRAM_ADDRESS=https://api.telegram.org
@REM SET TELEGRAM_BOT_TOKEN=""
@REM SET TELEGRAM_MESSAGE_FORMAT=Markdown

@REM SET BACKENDLESS_ADDRESS=https://*.ngrok.io
@REM SET BACKENDLESS_APP_ID=my-backendeless-app-id
@REM SET BACKENDLESS_APP_TOKEN=my-backendeless-token

@SET LOGGING_LEVEL=INFO
@SET LOGGING_CONSOLE=false

@cd .\src
@C:\OneScriptWeb\OneScript.WebHost.exe