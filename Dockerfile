FROM evilbeaver/onescript:1.5.0 AS onescript

COPY src /app
WORKDIR /app
#RUN opm install -l

FROM evilbeaver/oscript-web:0.7.0 
COPY --from=onescript /app .
WORKDIR /app